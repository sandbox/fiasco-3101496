<?php

namespace Drupal\acquia_authcache;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\Site\Settings;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Handle the metadata used to instruct Varnish to cache on Acquia Cloud.
 */
class AcquiaAuthCacheManager {

  /**
   * Name of Acquia AuthCache Key response header.
   */
  const HEADER_KEY = 'key';

  /**
   * Name of Acquia AuthCache Status response header.
   */
  const HEADER_STATUS = 'status';

  /**
   * Name of Acquia AuthCache header prefix.
   */
  const HEADER_PREFIX = 'x-acq-authc-';

  /**
   * The render cache.
   *
   * @var \Drupal\Core\Cache\Context\CacheContextsManager
   */
  protected $cacheContextsManager;

  /**
   * Constructs a new DynamicPageCacheSubscriber object.
   *
   * @param \Drupal\Core\Cache\Context\CacheContextsManager $cache_contexts_manager
   *   The cache contexts manager.
   */
  public function __construct(CacheContextsManager $cache_contexts_manager) {
    $this->cacheContextsManager = $cache_contexts_manager;
  }

  /**
   * Sign a value that it can be validated later.
   *
   * @param string $value
   *   The value to sign.
   */
  public function signValue($value) {
    return hash_hmac('md5', $value, $this->getKey());
  }

  /**
   * Determine is the value's signature is valid.
   *
   * @param string $value
   *   The value to sign.
   * @param string $signature
   *   The signature of the value.
   */
  public function isSignedValue($value, $signature) {
    return $this->signValue($value) === $signature;
  }

  /**
   * Get the cache contexts of a given account.
   *
   * @param \Drupal\user\UserInterface $account
   *   The account to pull cache contexts from.
   */
  public function getUserAccountContexts(UserInterface $account) {
    $context = [
      $this->hashValue('user') => $this->hashValue($account->uid->getValue()[0]['value']),
    ];
    foreach ($account->getRoles() as $role) {
      $context[$this->hashValue($role)] = $this->hashValue($role);
    }
    return str_replace('&', '|', http_build_query($context));
  }

  /**
   * Retrieve a list of cache content key/value pairs from a response.
   *
   * @param \Drupal\Core\Cache\CacheableResponseInterface $response
   *   The response whose cacheability to analyze.
   */
  public function getResponseContexts(CacheableResponseInterface $response) {
    // Extract user contexts.
    $context_cache_keys = $this->cacheContextsManager->convertTokensToKeys($response->getCacheableMetadata()->getCacheContexts());
    $encoded_keys = array_filter($context_cache_keys->getKeys(), function ($key) {
      return strpos($key, '[user]') === 0 || strpos($key, '[user.roles') === 0;
    });

    $keys = [];
    foreach ($encoded_keys as $encoded_key) {
      list($key, $value) = explode(']=', substr($encoded_key, 1), 2);

      if (strpos($key, 'user.roles:') !== FALSE) {
        if ($value === '0') {
          continue;
        }
        list($key, $value) = explode(':', $key);
        $key = $value;
      }
      $keys[$this->hashValue($key)] = $this->hashValue($value);
    }

    return $keys;
  }

  /**
   * Sign the Response with the AuthCache Key.
   *
   * @param \Drupal\Core\Cache\CacheableResponseInterface $response
   *   The response whose cacheability to analyze.
   * @param string $cache_outcome
   *   Indicate if the response should be cached or not.
   */
  public function signResponse(CacheableResponseInterface $response, $cache_outcome) {
    $response->headers->set(self::HEADER_PREFIX . self::HEADER_KEY, $this->getKey());

    $vary = $response->getVary();
    foreach ($this->getResponseContexts($response) as $key => $value) {
      $vary[] = self::HEADER_PREFIX . $key;
    }
    $response->setVary(implode(',', $vary));

    $control = 'no-store';
    if ($cache_outcome === 'STORE') {
      $config = \Drupal::service('config.factory')->get('acquia_authcache.settings');
      $control = 'max-age=' . $config->get('cache_ttl');
    }
    $control .= ', ' . strtolower($cache_outcome);
    $response->headers->set(self::HEADER_PREFIX . 'control', $control);
  }

  /**
   * Verify the request contains the correct metadata for AuthCache.
   *
   * @param array $contexts
   *   Array of cache contexts from the response.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request received by the client.
   */
  public function verifyRequest(array $contexts, Request $request) {
    foreach ($contexts as $key => $value) {
      $header_name = self::HEADER_PREFIX . $this->hashValue($key);

      // This would be an issue because the response needs to vary on a value
      // that wasn't present in the request.
      if (!$request->headers->get($header_name)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Get Acquia AuthCache Key.
   *
   * This is a unique key made up of the site UUID, hash_salt and Acquia
   * Environment.
   * This key is used to sign cookie data that can be validated by Varnish.
   */
  protected function getKey() {
    $uuid = \Drupal::config('system.site')->get('uuid');
    $salt = Settings::get('hash_salt');
    return hash("md5", $uuid . $salt . $_ENV['AH_ENVIRONMENT']);
  }

  /**
   * Hash a value to a 3-byte value.
   *
   * @param string $value
   *   The value to hash.
   */
  protected function hashValue($value) {
    return substr(hash('md5', $value . Settings::get('hash_salt')), 0, 3);
  }

}
