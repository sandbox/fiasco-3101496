<?php

namespace Drupal\acquia_authcache\EventSubscriber;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\PageCache\ResponsePolicyInterface;
use Drupal\Core\Render\RenderCacheInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\acquia_authcache\AcquiaAuthCacheManager;

/**
 * Returns cached responses as early and avoiding as much work as possible.
 *
 * Dynamic Page Cache is able to cache so much because it utilizes cache
 * contexts: the cache contexts that are present capture the variations of every
 * component of the page. That, combined with the fact that cacheability
 * metadata is bubbled, means that the cache contexts at the page level
 * represent the complete set of contexts that the page varies by.
 *
 * The reason Dynamic Page Cache is implemented as two event subscribers (a late
 * REQUEST subscriber immediately after routing for cache hits, and an early
 * RESPONSE subscriber for cache misses) is because many cache contexts can only
 * be evaluated after routing. (Examples: 'user', 'user.permissions', 'route' …)
 * Consequently, it is impossible to implement Dynamic Page Cache as a kernel
 * middleware that simply caches per URL.
 *
 * @see \Drupal\Core\Render\MainContent\HtmlRenderer
 * @see \Drupal\Core\Cache\CacheableResponseInterface
 */
class PageAuthCacheSubscriber implements EventSubscriberInterface {

  /**
   * A response policy rule determining the cacheability of the response.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicyInterface
   */
  protected $responsePolicy;

  /**
   * AuthCache Manager.
   *
   * @var \Drupal\acquia_authcache\AcquiaAuthCacheManager
   */
  protected $authCacheManager;

  /**
   * Constructs a new DynamicPageCacheSubscriber object.
   *
   * @param \Drupal\Core\PageCache\ResponsePolicyInterface $response_policy
   *   A policy rule determining the cacheability of the response.
   * @param \Drupal\acquia_authcache\AcquiaAuthCacheManager $authcache_manager
   *   The render cache.
   */
  public function __construct(ResponsePolicyInterface $response_policy, AcquiaAuthCacheManager $authcache_manager) {
    $this->responsePolicy = $response_policy;
    $this->authCacheManager = $authcache_manager;
  }

  /**
   * Stores a response in case of a Dynamic Page Cache miss, if cacheable.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The event to process.
   */
  public function onResponse(FilterResponseEvent $event) {
    $response = $event->getResponse();
    $request = $event->getRequest();
    $cache_outcome = 'STORE';

    // Indicate to Acquia Varnish that this response should not be cached.
    if (!$response instanceof CacheableResponseInterface) {
      return;
    }
    // Don't cache the response if the AuthCache response policies are not met.
    elseif ($this->responsePolicy->check($response, $request) === ResponsePolicyInterface::DENY) {
      $cache_outcome = 'ADMIN';
    }

    $contexts = $this->authCacheManager->getResponseContexts($response);

    if (!$this->authCacheManager->verifyRequest($contexts, $request)) {
      $cache_outcome = 'INCOMPATIBLE';
    }

    $header_name = AcquiaAuthCacheManager::HEADER_PREFIX . AcquiaAuthCacheManager::HEADER_STATUS;

    $response->headers->set($header_name, $cache_outcome);
    $this->authCacheManager->signResponse($response, $cache_outcome);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];

    // Run after HtmlResponseSubscriber::onRespond(), which has priority 0.
    $events[KernelEvents::RESPONSE][] = ['onResponse', -1];

    return $events;
  }

}
